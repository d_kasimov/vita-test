<?php

if ('application/json' == $_SERVER['CONTENT_TYPE'] && 'POST' == $_SERVER['REQUEST_METHOD'])
{
    $data = json_decode(file_get_contents('php://input'), true);

    if (!empty($data) && isset($data['pharmacies']) && is_array($data['pharmacies'])) {
        $fileName = '../../upload/upload' . (new \DateTime())->format('dmY_i:H') . 'csv';

        try {
            $fp = fopen($fileName, 'w');

            if (!$fp) {
                throw new \Exception('Ошибка записи файла');
            }

            foreach ($data['pharmacies'] as $pharmacy) {
                $resRecord = fputcsv($fp, $pharmacy);

                if (!$resRecord) {
                    throw new \Exception('Ошибка записи файла');
                }
            }

            fclose($fp);
        } catch (\Exception $e) {
            echo json_encode(['success' => false, 'data' => $e->getMessage()]);
        }

        echo json_encode(['success' => true, 'data' => 'Файл успешно записан']);
    }
}