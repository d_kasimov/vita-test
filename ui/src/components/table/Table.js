import React from 'react';
import BootstrapTable from 'react-bootstrap-table-next';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import './table.less';
import 'bootstrap/dist/css/bootstrap.min.css';
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import FormControl from "react-bootstrap/FormControl";
import FormLabel from "react-bootstrap/FormLabel";
import FormGroup from "react-bootstrap/FormGroup";
import {saveTable} from "../../services/rest";

const columns = [{
    dataField: 'id',
    text: 'ID',
    sort: true
}, {
    dataField: 'name',
    text: 'Название',
    sort: true
}, {
    dataField: 'address',
    text: 'Адрес',
    sort: true
}, {
    dataField: 'button',
    text: '',
}
];

const pharmacies = [
    {
        id: 1,
        name: 'Аптека #213',
        address: 'Куйбышева 110',
    },
    {
        id: 2,
        name: 'Аптека #214',
        address: 'Куйбышева 111',
    },
    {
        id: 3,
        name: 'Аптека #215',
        address: 'Куйбышева 112',
    },
];

const defaultPharmacy = {
    id: null,
    name: '',
    address: ''
}

export default class Table extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            pharmacies: pharmacies,
            editDialog: false,
            editPharmacy: defaultPharmacy,
            responseDialog: false,
            responseMessage: null
        }
    }

    render() {
        const {pharmacies, editDialog, editPharmacy, responseDialog, responseMessage} = this.state;

        return (
            <React.Fragment>
                <BootstrapTable
                    keyField='id'
                    data={pharmacies.map(pharmacy => {
                        return {
                            ...pharmacy,
                            button: <span className={'editLink'}
                                          onClick={this.handleEditPharmacy(pharmacy.id)}>Редактировать</span>
                        };
                    })
                    }
                    columns={columns}
                    selectRow={{
                        mode: 'checkbox',
                        onSelect: this.clickSelect,
                        hideSelectAll: true
                    }}
                />
                <div className={'button__container'}>
                    <Button variant="primary" className={'button'} onClick={this.handleClickDel} >Удалить</Button>
                    <Button variant="primary" className={'button'} onClick={this.handleClickAdd} >Добавить</Button>
                    <Button variant="primary" className={'button'} onClick={this.handleSaveTable} >Выгрузить в файл</Button>
                </div>
                <Modal show={editDialog} onHide={this.handleEditDialog}>
                    <Modal.Header closeButton>
                        <Modal.Title>Редактирование аптеки</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <FormGroup controlId="name">
                            <FormLabel>Аптека</FormLabel>
                            <FormControl
                                type="text"
                                value={editPharmacy && editPharmacy.name}
                                onChange={this.handleChange('name')}
                            />
                        </FormGroup>
                        <FormGroup controlId="address">
                            <FormLabel>Адрес</FormLabel>
                            <FormControl
                                type="text"
                                value={editPharmacy && editPharmacy.address}
                                onChange={this.handleChange('address')}
                            />
                        </FormGroup>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="primary" onClick={this.handleSaveEditDialog}>
                            Сохранить
                        </Button>
                    </Modal.Footer>
                </Modal>
                <Modal show={responseDialog} onHide={this.handleResponseDialog}>
                    <Modal.Header closeButton>
                        <Modal.Title>Выгрузка</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        {responseMessage}
                    </Modal.Body>
                </Modal>
            </React.Fragment>
        )
    }

    handleEditPharmacy = (pharmacyId = null) => () => {
        const {pharmacies, editDialog} = this.state;
        const editPharmacy = pharmacies.find(pharmacy => pharmacy.id === pharmacyId);

        this.setState({
            editDialog: !editDialog,
            editPharmacy: {...editPharmacy}
        })
    }

    handleEditDialog = () => {
        this.setState({
            editDialog: !this.state.editDialog
        })
    }

    handleChange = (property) => (event) => {
        const {editPharmacy} = this.state;

        if (!editPharmacy) {
            return;
        }

        editPharmacy[property] = event.target.value;

        this.setState({
            editPharmacy
        })
    }

    handleSaveEditDialog = () => {
        const {pharmacies, editPharmacy, editDialog} = this.state;

        if (!editPharmacy) {
            return;
        }

        let newElement = true;
        let processedPharmacies = pharmacies.map(pharmacy => {
            if ( pharmacy.id !== editPharmacy.id) {
                return pharmacy;
            }

            newElement = false;

            return {...pharmacy, name: editPharmacy.name, address: editPharmacy.address};
        });

        if (newElement) {
            pharmacies.push({...editPharmacy});
        }

        this.setState({
            editDialog: !editDialog,
            pharmacies: newElement ? pharmacies : processedPharmacies
        })
    }

    clickSelect = (pharmacy) => {
        const {pharmacies} = this.state;

        this.setState({
            pharmacies: pharmacies.map(itemPharmacy => {
                if (itemPharmacy.id !== pharmacy.id) {
                    return itemPharmacy;
                }

                return itemPharmacy.selected ? {...itemPharmacy, selected: !itemPharmacy.selected} : {
                    ...itemPharmacy,
                    selected: true
                };
            })
        })
    }

    handleClickDel = () => {
        this.setState({
            pharmacies: this.state.pharmacies.filter(pharmacy => !pharmacy.selected)
        })
    }

    handleClickAdd = () => {
        this.setState({
            editPharmacy: {...defaultPharmacy, id: this.state.pharmacies.length + 1},
            editDialog: true,
        })
    }

    handleSaveTable = () => {
        const {pharmacies} = this.state;

        saveTable(pharmacies)
            .then(response => {
                if (response.success) {
                    this.handleResponseDialog(response.data);
                }
            })
    }

    handleResponseDialog = (responseMessage = null) => {
        const { responseDialog } = this.state;

        this.setState({
            responseDialog: !responseDialog,
            responseMessage: responseMessage
        })
    }
}