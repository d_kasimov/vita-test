import React from 'react';
import ReactDOM from 'react-dom';
import Table from './components/table/Table';

ReactDOM.render(
    React.createElement(Table),
    document.getElementById('table')
);