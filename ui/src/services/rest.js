import baseRequest from "./baserequest";

export function saveTable (pharmacies) {
    return baseRequest('/api/save-pharmacies/', {pharmacies});
}