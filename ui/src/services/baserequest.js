import * as queryString from "query-string-object";

export default async function baseRequest(url, data) {
    let params = {};
    params.headers = new Headers();
    params.headers.set('Content-Type', 'application/json');
    params.body = JSON.stringify(data);
    params.method = 'POST';

    const request = new Request(url, params);
    let response = await fetch(request);

    return response.json();
}